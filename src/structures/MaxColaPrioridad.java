package structures;

import java.util.Iterator;
import structures.*;

import org.apache.commons.lang3.SystemUtils;

public class MaxColaPrioridad<T extends Comparable<T>> implements IColaPrioridad<T>
{
	private Nodo<T> primero;
	private int numElementos; 
	private IteratorLista2<T> iterator;

	public MaxColaPrioridad ()	
	{
		primero=null;
		numElementos=0;
		iterator= new IteratorLista2<T>(primero);
	}

	public Iterator<T> iterator() 
	{
		return iterator;
	}

	public int darNumElementos() 
	{
		return numElementos;
	}

	public void agregar(T elemento) 
	{		
		Nodo<T> nuevo=new Nodo<T>(elemento);
		if(primero==null)
		{
			primero=nuevo;
		}

		else
		{
			Nodo<T> actual=primero;
			while(actual!=null)
			{
				if(nuevo.darElemento().compareTo(actual.darElemento())>0)
				{
					if(actual==primero)
					{
						primero.cambiarAnterior(nuevo);
						nuevo.cambiarSiguiente(primero);
						primero=nuevo;
					}
					else
					{
						nuevo.cambiarSiguiente(actual);
						nuevo.cambiarAnterior(actual.darAnterior());
						actual.darAnterior().cambiarSiguiente(nuevo);
						actual.cambiarAnterior(nuevo);
					}
					break;
				}

				else if(actual.darElemento().compareTo(nuevo.darElemento())==0)
				{
					if(actual.darAnterior()==null)
					{
						if(actual.darSiguiente()!=null)
						{
							actual.darSiguiente().cambiarAnterior(null);
						}
						primero=actual.darSiguiente();
					}

					else if(actual.darSiguiente()==null)
					{
						actual.darAnterior().cambiarSiguiente(null);
					}

					else
					{
						actual.darAnterior().cambiarSiguiente(actual.darSiguiente());
						actual.darSiguiente().cambiarAnterior(actual.darAnterior());
					}
					numElementos-=2;
					agregar(nuevo.darElemento());
					break;
				}

				else
				{
					if(actual.darSiguiente()!=null)
					{
						actual=actual.darSiguiente();
					}
					else
					{
						actual.cambiarSiguiente(nuevo);
						nuevo.cambiarAnterior(actual);
						break;
					}
				}
			}
		}
		numElementos++;
	}

	public T delMax() 
	{
		Nodo<T> eliminado=null;
		if(numElementos!=0)
		{
			eliminado=primero;
			primero=primero.darSiguiente();
		}
		else 
			return null;
		numElementos--;
		return eliminado.darElemento();
	}

	public T darElemento(int i)
	{
		int avance = 0;
		Nodo<T> actual = primero;
		while( actual!=null && i>avance)
		{
			actual = actual.darSiguiente();
			avance++;
		}

		if(actual!=null)
		{
			return actual.darElemento();
		}

		else 
			return null;
	}
	public boolean contains(T e)
	{
		Nodo<T> porEncontrar = new Nodo<T>(e);
		Nodo<T> actual = primero; 
		boolean encontrado = false; 
		while(actual != null && !encontrado)
		{
			if(actual.equals(porEncontrar))
			{
				return true;
			}
		}
		return false; 
	}

	public T max() 
	{
		return primero.darElemento();
	}

	public boolean esVacia() 
	{
		return numElementos==0;
	}
}
