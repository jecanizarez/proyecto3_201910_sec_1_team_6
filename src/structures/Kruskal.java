package structures;

import structures.Queue;
import mundo.Vertex;
import mundo.Way;
import structures.ColaPrioridad;
import structures.Graph;
import structures.ArregloDinamico;

import java.util.Comparator;

import mundo.Edge;
import mundo.Intersection;
import mundo.Way;

public class Kruskal 
{

	private double weight; //weight of MST
	private Queue<Edge<Way>> mst = new Queue<Edge<Way>>();  //edges in MST

	/**
	 * Compute a minimum spanning tree (or forest) of an edge-weighted graph.
	 * @param G the edge-weighted graph
	 */
	@SuppressWarnings("unchecked")
	public Kruskal(Graph<Long, Intersection, Way> G)
	{
		ColaPrioridad<Edge<Way>> pq = new ColaPrioridad<Edge<Way>>(new comparatorDistancia());
		ArregloDinamico<Edge<Way>> arcoArreglos = (ArregloDinamico<Edge<Way>>) G.getEdges();
		for (int i = 0; i < arcoArreglos.darTamano(); i++) 
		{
			Edge<Way> arcoA = arcoArreglos.darElemento(i);
			pq.agregar(arcoA);
		}

		while (!pq.esVacia() && mst.size()< G.V() - 1) 
		{
			Edge<Way> e = pq.delMax();
			Vertex<Long,Intersection,Way>  v = e.getV1();
			Vertex<Long,Intersection,Way> w = e.getV2();
			mst.enqueue(e);
			weight += e.getInfo().getDistance();

		}
		
	}

	/**
	 * Returns the edges in a minimum spanning tree (or forest).
	 * @return the edges in a minimum spanning tree (or forest) as
	 *    an iterable of edges
	 */
	public Iterable<Edge<Way>> edges() {
		return mst;
	}

	/**
	 * Returns the sum of the edge weights in a minimum spanning tree (or forest).
	 * @return the sum of the edge weights in a minimum spanning tree (or forest)
	 */
	public double weight() {
		return weight;
	}
	public class comparatorDistancia implements Comparator<Edge<Way>>
	{

		@Override
		public int compare(Edge<Way> o1, Edge<Way> o2) {
			// TODO Auto-generated method stub
			return (int)o1.getInfo().getDistance() -(int)o2.getInfo().getDistance();
		}
	}

}