package structures;

import java.util.Iterator;

public class IteratorLista2<T> implements Iterator
{

	private Nodo<T> proximo; 

	private Nodo<T> ant_prox; 

	public IteratorLista2(Nodo<T> primero)
	{
		proximo = primero; 
	}

	public boolean hasNext() {
		// TODO Auto-generated method stub
		return proximo != null; 
	}

	public T next() 
	{
		// TODO Auto-generated method stub
		T retornar = proximo.darElemento();
		ant_prox = proximo;
		proximo = proximo.darSiguiente(); 
		return retornar; 
	}
}
