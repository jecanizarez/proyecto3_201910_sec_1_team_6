package structures;
import structures.*;
import mundo.*;




public class Prim {
	private boolean[] marked; 
	private Queue<Edge<Way>> mst;
	private MinPQ<Edge> pq;
  public Prim(Graph G) {
	  pq = new MinPQ<Edge>();
	  mst = new Queue<Edge<Way>>(); 
	  marked = new boolean[G.getVertex().darCapacidad()]; 
	  visit(G , 0);
	  while(!pq.isEmpty() && mst.size() < G.V() -1)
	  {
		  Edge<Way> e = pq.delMin(); 
		  Vertex<Long, Intersection, Way> v = e.getV1(), w = e.getV2();
		  if(marked[G.getVertex().getI(v.getId())] && marked[G.getVertex().getI(w.getId())])
		  {
			  continue; 
		  }
		  mst.enqueue(e);
		  if(!marked[G.getVertex().getI(v.getId())])
		  {
			  visit(G, G.getVertex().getI(v.getId())); 
		  }
		  if(!marked[G.getVertex().getI(w.getId())])
		  {
			  visit(G, G.getVertex().getI(w.getId())); 
		  }
	  }
	  
	  
  }
  private void visit(Graph<Long, Intersection, Way> G, int v)
  {
	  Vertex<Long, Intersection, Way> vertex = G.getVertex().getValues().darElemento(v);
	  marked[G.getVertex().getI(vertex.getId())] = true;
	  for(Edge<Way> e: vertex.getAdj())
	  {
		  Vertex<Long, Intersection, Way> other; 
		  if(e.getV1().getId().equals(vertex.getId()))
		  {
			other = e.getV2();   
		  }
		  else
		  {
			  other = e.getV1(); 
		  }
		  if(!marked[G.getVertex().getI(other.getId())])
		  {
			  pq.insert(e);
		  }

	  }
	  
  }
  public Iterable<Edge<Way>> mst()
  {
	  return mst;
  }
  
  
}
