package model.util;

import mundo.Edge;
import mundo.Intersection;
import mundo.Vertex;
import mundo.Way;
import structures.ArregloDinamico;
import structures.Graph;
import structures.IndexMinPQ;
import structures.LinearHash;

public class Dijkstra {
	private LinearHash<Long,Double> distTo;          // distTo[v] = distance  of shortest s->v path
	private LinearHash<Long,Long> edgeTo;    // edgeTo[v] = last edge on shortest s->v path
	private IndexMinPQ<Double> pq;    // priority queue of vertices
	private LinearHash<Long,Vertex<Long,Intersection,Way>> vertices;

	public Dijkstra(Graph G, Long s) {
		for (int i =0;i<G.getEdges().darTamano();i++)
		{
			Edge<Way> e=(Edge<Way>) G.getEdges().darElemento(i);
			if (e.getInfo().getDistance() < 0)
				throw new IllegalArgumentException("edge " + e + " has negative weight");
		}
		LinearHash<Long,Vertex<Long,Intersection,Way>> vertices= G.getVertex();
		distTo = new LinearHash<Long,Double>(G.V());
		edgeTo = new LinearHash<Long,Long>(G.V());
		Long v;
		for ( v = (long) 0; v < G.V(); v++)
			distTo.put(v, Double.POSITIVE_INFINITY);
		distTo.put(s, 0.0);

		// relax vertices in order of distance from s
		pq = new IndexMinPQ<Double>(G.V());
		pq.insert(s, distTo.get(s));
		while (!pq.isEmpty()) {
			Double v = pq.delMin();
			for (Edge<Way> e : G.adja(v))
				relax(e);
		}
	}

	// relax edge e and update pq if changed
	private void relax(Edge<Way> e)
	{
		Long v = (Long) e.getV1().getId(), w = (Long) e.getV2().getId();
		if (distTo.get(w) > distTo.getI(v) + e.getInfo().getDistance()) 
		{
			distTo.put(w, distTo.get(v) + e.getInfo().getDistance());
			edgeTo[w] = e;
			if 
			(pq.contains(w)) pq.decreaseKey(w, distTo.get(w));
			else                
				pq.insert(w, distTo.get(w));
		}
	}

	public double distTo(Long v) 
	{
		return distTo.get(v);
	}

	public boolean hasPathTo(Long v) 
	{
		return distTo.get(v) < Double.POSITIVE_INFINITY;
	}

	public ArregloDinamico <Edge<Way>> pathTo(Long v)
	{
		if (!hasPathTo(v)) return null;
		ArregloDinamico<Edge<Way>> path = new ArregloDinamico<Edge<Way>>(10);
		for (Edge<Way> e = vertices.get(edgeTo.get(v)); e != null; e = vertices.get(edgeTo.get(e.getV1().getId())))
		{
			path.agregar(e);
		}
		return path;
	}
}