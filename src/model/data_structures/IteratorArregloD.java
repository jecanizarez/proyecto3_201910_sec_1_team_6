package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

import structures.ArregloDinamico;

public class IteratorArregloD<T extends Comparable<T>> implements Iterator<T>
{

	private ArregloDinamico<T> arreglo;
	private int contador =0;

	public IteratorArregloD( ArregloDinamico<T> pArreglo)
	{
		arreglo = pArreglo;
	}

	public boolean hasNext( )
	{
		return arreglo.darElemento(contador) != null;
	}

	public T next( )
	{
		if( arreglo.darElemento(contador) == null )
		{ 
			throw new NoSuchElementException("No hay proximo"); 
		}
		T elemento = arreglo.darElemento(contador); 
		contador++;
		return elemento;
	}
}